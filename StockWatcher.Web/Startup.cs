﻿using System;
using System.Threading.Tasks;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(StockWatcher.Web.Startup))]
namespace StockWatcher.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // wire up signalR
            app.MapSignalR();
        }
    }
}