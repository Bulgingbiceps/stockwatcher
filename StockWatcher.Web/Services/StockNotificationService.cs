﻿using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using StockWatcher.Core.Interfaces;
using StockWatcher.Core.Models;
using StockWatcher.Web.Hubs;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;


namespace StockWatcher.Web.Services

{
    [Export(typeof(IStockNotificationService))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class StockNotificationService : IStockNotificationService
    {
        private const string NotificationKey = "Eq1";

        static Lazy<IHubContext> hub = new Lazy<IHubContext>(() => GlobalHost.ConnectionManager.GetHubContext<StockHub>());

        protected IHubContext Hub
        {
            get { return hub.Value; }

        }
        public void NoftityClients(IEnumerable<Stock> stocks)
        {
            var subscribed = Hub.Clients.Group(NotificationKey);
            subscribed.refreshItems(stocks);
        }

    }
}
