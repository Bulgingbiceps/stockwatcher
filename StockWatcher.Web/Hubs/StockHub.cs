﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace StockWatcher.Web.Hubs
{
    public class StockHub : Hub
    {
        public void Subscribe(string packageId)
        {
            Groups.Add(Context.ConnectionId, packageId);
        }

        public void Unsubscribe(string packageId)
        {
            Groups.Remove(Context.ConnectionId, packageId);
        }
    }
}