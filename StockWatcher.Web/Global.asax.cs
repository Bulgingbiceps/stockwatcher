﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;
using System.Web.Http;
using System.ComponentModel.Composition.Hosting;
using System.Reflection;
using System.ComponentModel.Composition;
using System.Web.Http.Dependencies;
using System.IO;

namespace StockWatcher.Web
{
    public class Global : HttpApplication
    {
        void Application_Start(object sender, EventArgs e)
        {
            // Code that runs on application startup
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);


            MefConfig.RegisterMef();
            var catalog = new AssemblyCatalog(Assembly.GetExecutingAssembly());
            var composition = new CompositionContainer(catalog);
            IControllerFactory mefControllerFactory = new MefControllerFactory(composition); //Get Factory to be used
            ControllerBuilder.Current.SetControllerFactory(mefControllerFactory); //Set Factory to be used
        }

        public class MefControllerFactory : DefaultControllerFactory
        {
            private readonly CompositionContainer _container; // This container will work like an IOC container
            public MefControllerFactory(CompositionContainer container)
            {
                _container = container;
            }
            protected override IController GetControllerInstance(System.Web.Routing.RequestContext requestContext, Type controllerType)
            {
                Lazy<object, object> export = _container.GetExports(controllerType, null, null).FirstOrDefault();

                //here if the controller object is not found (resulted as null) we can go ahead and get the default controller.
                //This means that in order to get the Controller we have to Export it first which will be shown later in this post
                return null == export ? base.GetControllerInstance(requestContext, controllerType) : (IController)export.Value;
            }
            public override void ReleaseController(IController controller)
            {
                //this is were the controller gets disposed
                ((IDisposable)controller).Dispose();
            }
        }

        /// <summary>
        /// Resolve dependencies for MVC / Web API using MEF.
        /// </summary>
        public class MefDependencyResolver : System.Web.Http.Dependencies.IDependencyResolver, System.Web.Mvc.IDependencyResolver
        {
            private readonly CompositionContainer _container;

            public MefDependencyResolver(CompositionContainer container)
            {
                _container = container;
            }

            public IDependencyScope BeginScope()
            {
                return this;
            }

            /// <summary>
            /// Called to request a service implementation.
            /// 
            /// Here we call upon MEF to instantiate implementations of dependencies.
            /// </summary>
            /// <param name="serviceType">Type of service requested.</param>
            /// <returns>Service implementation or null.</returns>
            public object GetService(Type serviceType)
            {
                if (serviceType == null)
                    throw new ArgumentNullException("serviceType");

                var name = AttributedModelServices.GetContractName(serviceType);
                var export = _container.GetExportedValueOrDefault<object>(name);
                return export;
            }

            /// <summary>
            /// Called to request service implementations.
            /// 
            /// Here we call upon MEF to instantiate implementations of dependencies.
            /// </summary>
            /// <param name="serviceType">Type of service requested.</param>
            /// <returns>Service implementations.</returns>
            public IEnumerable<object> GetServices(Type serviceType)
            {
                if (serviceType == null)
                    throw new ArgumentNullException("serviceType");

                var exports = _container.GetExportedValues<object>(AttributedModelServices.GetContractName(serviceType));
                return exports;
            }

            public void Dispose()
            {
            }
        }

        public static class MefConfig
        {
            public static void RegisterMef()
            {
                AggregateCatalog aggregatecatalogue = new AggregateCatalog();
                aggregatecatalogue.Catalogs.Add(new DirectoryCatalog(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "bin")));
                CompositionContainer container = new CompositionContainer(aggregatecatalogue);

                var resolver = new MefDependencyResolver(container);
                // Install MEF dependency resolver for MVC
                DependencyResolver.SetResolver(resolver);
                // Install MEF dependency resolver for Web API
                System.Web.Http.GlobalConfiguration.Configuration.DependencyResolver = resolver;
            }
        }
    }
}