﻿using StockWatcher.Core.Interfaces;
using StockWatcher.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace StockWatcher.Web.Controllers
{
    [Export]
    [PartCreationPolicy(CreationPolicy.NonShared)]
    public class StockController : ApiController
    {
        [Import]
        public IStockDataService StockDataService;

        public IEnumerable<Stock> Get()
        {
            return StockDataService.GetStocks();
        }
    }
}
