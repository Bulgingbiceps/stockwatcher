﻿(function () {
    var app = angular.module('myApp', []),
        uri = 'api/stock/stocks',
        errorMessage = function (data, status) {
            return 'Error: ' + status +
                (data.Message !== undefined ? (' ' + data.Message) : '');
        },
        hub = $.connection.stockHub; // create a proxy to signalr hub on web server

    app.controller('myCtrl', ['$http', '$scope', function ($http, $scope) {
        $scope.stocks = [];
        //This is the stock packageid, essentially the key for getting notification for the selected stocks
        $scope.stockSubscriptionPackageId = 'Eq1'
        $scope.stockpackageIdSubscribed;
        $scope.stockIdSearch;

        //load stock data and subscribe to signalR notification
        $scope.getAllStocks = function () {
            $http.get(uri)
                .success(function (data, status) {
                    // show current stocks
                    $scope.stocks = data; 

                    //start signalr hub
                    $.connection.hub.start()
                        .done(function () {
                            hub.server.subscribe($scope.stockSubscriptionPackageId);
                            $scope.stockpackageIdSubscribed = $scope.stockSubscriptionPackageId;
                        })
                        .fail(function () {
                            //even if signalr fails, manual refresh will still work
                            $scope.errorToSearch = "Could not connect to real Time service. Please manually refresh";
                        });
                })
                .error(function (data, status) {
                    $scope.stocks = [];
                    $scope.errorToSearch = errorMessage(data, status);
                })
        };

        //refresh stock data
        $scope.refresh = function () {
            $http.get(uri)
                .success(function (data, status) {
                    $scope.stocks = data;
                })
                .error(function (data, status) {
                    $scope.stocks = [];
                    $scope.errorToSearch = errorMessage(data, status);
                })
        };

        $scope.toShow = function () { return $scope.stocks && $scope.stocks.length > 0; };

        // on page load set order property by stock Id
        $scope.orderProp = 'Id';

        hub.client.refreshItems = function (items) {
            $scope.stocks = items;
            $scope.$apply();
        }
    }]);
})();
