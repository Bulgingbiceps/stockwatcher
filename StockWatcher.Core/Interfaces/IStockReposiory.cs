﻿using StockWatcher.Core.Models;
using System.Collections.Generic;

namespace StockWatcher.Core.Interfaces
{
    /// <summary>
    /// Interface for the stock Repository
    /// </summary>
    public interface IStockReposiory
    {
        IEnumerable<Stock> GetStocks();
    }
}
