﻿using StockWatcher.Core.Models;
using System;
using System.Collections.Generic;


namespace StockWatcher.Core.Interfaces
{
    /// <summary>
    /// Interface getting  all the stock data.
    /// </summary>
    public interface IStockDataService
    {
        IEnumerable<Stock> GetStocks();
    }
}
