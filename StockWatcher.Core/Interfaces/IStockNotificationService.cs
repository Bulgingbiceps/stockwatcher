﻿using System.Collections.Generic;
using StockWatcher.Core.Models;

namespace StockWatcher.Core.Interfaces
{
    /// <summary>
    /// Interface for notification
    /// </summary>
    public interface IStockNotificationService
    {
        void NoftityClients(IEnumerable<Stock> stocks);
    }
}
