﻿using StockWatcher.Core.Models;
using System.Collections.Generic;

namespace StockWatcher.Core.Interfaces
{
    /// <summary>
    /// Interface for Stock Simulation
    /// </summary>
    public interface IStockSimulationService
    {
        IEnumerable<Stock> SimulatePrice(IEnumerable<Stock> stocks);

    }
}