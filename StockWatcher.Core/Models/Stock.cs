﻿namespace StockWatcher.Core.Models
{
    /// <summary>
    /// Model class Representing Stock
    /// </summary>
    public class Stock
    {
        public string Id { get; set; }

        public decimal Price { get; set; }
    }
}