**This is a stock watcher demo application.** 
The Library consists of services for generating mock stock tickers. The simulation service changes the prices to mock a real world use case of changing prices.
Other details
![StockwatcherLib.jpg](https://bitbucket.org/repo/64zqjXn/images/1859573663-StockwatcherLib.jpg)

The UI fetches the stocks using webapi. It also connects to signalR hub and the prices are reflected real time on the UI.
![StockWatcherScreen2.png](https://bitbucket.org/repo/64zqjXn/images/2376373272-StockWatcherScreen2.png)
![StockWatcherScreen3.png](https://bitbucket.org/repo/64zqjXn/images/3652398947-StockWatcherScreen3.png)