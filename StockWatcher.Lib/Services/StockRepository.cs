﻿using StockWatcher.Core.Interfaces;
using StockWatcher.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;

namespace StockWatcher.Lib.Services
{
    /// <summary>
    /// Its a mock repository for generating stock Names and Prices
    /// </summary>
    [Export(typeof(IStockReposiory))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class StockRepository:IStockReposiory
    {
        const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        public  IEnumerable<Stock> GetStocks()
        {
            var stocks = Enumerable.Range(1, 50).Select(x => new Stock { Id = RandomString(3), Price = random.Next(1,100) }).ToList(); ;
            return stocks;
        }
        private static Random random = new Random();
        public static string RandomString(int length)
        {
            
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
