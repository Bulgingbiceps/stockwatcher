﻿using StockWatcher.Core.Interfaces;
using StockWatcher.Core.Models;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Threading;

namespace StockWatcher.Lib.Services
{
    /// <summary>
    /// Reposible for getting and updating all the stock data. Uses Simulation and Notification service
    /// Implementation can be changed to handle notifications separately
    /// </summary>
    [Export(typeof(IStockDataService))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class StockDataService : IStockDataService
    {
        private const int SimulationTime = 5000;
        private const int SimulationDelay = 2000;
        Timer timer;

        [Import]
        public IStockSimulationService StockSimulationService;
        
        private static IEnumerable<Stock> Stocks;

        [Import]
        public IStockReposiory StockRepository { get; set; }

        [Import]
        public IStockNotificationService StockNotificationService;

        public IEnumerable<Stock> GetStocks()
        {
            if (Stocks == null || !Stocks.Any())
            {
                Stocks = StockRepository.GetStocks();
                if (timer == null)
                    timer = new Timer(StockSimulationHandler, null, SimulationDelay, SimulationTime);
            }
            return Stocks;
        }

        private void StockSimulationHandler(object state)
        {
            Stocks = StockSimulationService.SimulatePrice(Stocks);
            StockNotificationService.NoftityClients(Stocks);
        }
    }
}
