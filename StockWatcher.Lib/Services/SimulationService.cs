﻿using StockWatcher.Core.Interfaces;
using StockWatcher.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;

namespace StockWatcher.Lib.Services
{
    /// <summary>
    /// Mock service for simulating stock price
    /// </summary>
    [Export(typeof(IStockSimulationService))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class SimulationService : IStockSimulationService
    {
        public IEnumerable<Stock> SimulatePrice(IEnumerable<Stock> stocks)
        {
            if (stocks != null)
            {
                foreach (var stockitem in stocks)
                {
                    var change = Math.Round((decimal)(new Random().NextDouble()) * (new Random().Next(-10, 10)), 3, MidpointRounding.AwayFromZero);
                    stockitem.Price = stockitem.Price + (stockitem.Price < 30 ? Math.Abs(change) : change);
                }
            }
            return stocks;
        }
    }
}